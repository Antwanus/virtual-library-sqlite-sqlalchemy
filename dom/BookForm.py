from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired

class BookForm(FlaskForm):
    name = StringField(
        label='name',
        validators=[DataRequired()],
        description="name of the book"
    )
    author = StringField(
        label='author',
        validators=[DataRequired()],
        description="author of the book"
    )
    rating = StringField(
        label='rating',
        validators=[DataRequired()],
        description="rating of the book"
    )
    submit = SubmitField(
        label='Add'
    )
