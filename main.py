import os
from flask import Flask, render_template, request, redirect, url_for, send_from_directory
from flask_bootstrap import Bootstrap
from DataServiceSqlite import DataServiceSqlite
from dom.BookForm import BookForm

# FLASK
app = Flask(__name__)
app.secret_key = "yolo"
Bootstrap(app)

# DATASOURCE
data_service = DataServiceSqlite()

# ROUTES
@app.route('/')
def home():
    return render_template(template_name_or_list='index.html',
                           all_books=data_service.get_all_books())

@app.route("/add", methods=['GET', 'POST'])
def add():
    bf = BookForm()
    if bf.validate_on_submit():
        data_service.create_book(bf.name.data, bf.author.data, bf.rating.data)

    return render_template(template_name_or_list='add.html', form=bf)

@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'), 'favicon.ico',  mimetype='image/vnd.microsoft.icon')


if __name__ == "__main__":
    app.run(debug=True)
