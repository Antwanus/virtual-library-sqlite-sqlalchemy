import sqlite3


class DataServiceSqlite:
    DB_NAME = 'db/books-collection.db'

    def create_books_table(self) -> None:
        with sqlite3.connect(self.DB_NAME) as connection:
            cursor = connection.cursor()
            cursor.execute("CREATE TABLE IF NOT EXISTS books("
                           "id INTEGER PRIMARY KEY, "
                           "title varchar(250) NOT NULL UNIQUE, "
                           "author varchar(250) NOT NULL, "
                           "rating FLOAT NOT NULL)")

    def get_all_books(self) -> list:
        with sqlite3.connect(self.DB_NAME) as connection:
            cursor = connection.cursor()
            return [{
                'title': tuple4[1],
                'author': tuple4[2],
                'rating': tuple4[3]
            }
                for tuple4 in cursor.execute('SELECT b.id, b.title, b.author, b.rating FROM books AS b').fetchall()]

    def create_book(self, title, author, rating):
        with sqlite3.connect(self.DB_NAME) as connection:
            cursor = connection.cursor()
            values = (title, author, rating)
            cursor.execute('INSERT INTO books (title, author, rating) VALUES (?, ?, ?)', values)
            connection.commit()
